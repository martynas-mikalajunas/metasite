﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Task.Web.Data;
using Task.Web.Services;

namespace Task.Web
{
	public class Startup
	{
		private readonly IConfiguration _config;

		public Startup(IConfiguration config)
		{
			_config = config;
		}
		
		public void ConfigureServices(IServiceCollection services)
		{
			var cnnStr = _config.GetConnectionString("default");
			
			services.AddTransient<IReportService, DapperReportService>();
			services.AddTransient<IDbConnectionProvider, DbConnectionProvider>(
				provider => new DbConnectionProvider(cnnStr));
			services.AddTransient<IReportGenerator, ReportGenerator>();
			services.AddMvc();
			services.AddDbContext<ReportDbContext>(o => o.UseSqlServer(cnnStr));
		}

		public void Configure(IApplicationBuilder app, IHostingEnvironment env)
		{
			if (env.IsDevelopment())
			{
				app.UseDeveloperExceptionPage();
			}

			app.UseStaticFiles();
			app.UseMvc(r => r.MapRoute("default", "{controller=report}/{action=ByDate}"));
		}
	}
}