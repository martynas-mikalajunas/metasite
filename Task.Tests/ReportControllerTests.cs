﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Threading = System.Threading.Tasks;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using NSubstitute;
using Task.Web.Controllers;
using Task.Web.Models;
using Task.Web.Services;
using Xunit;

namespace Task.Tests
{
	public class ReportControllerTests
	{
		private readonly ReportController _sut;
		private readonly IReportService _reportService;
		private readonly IReportGenerator _reportGenerator;

		public ReportControllerTests()
		{
			_reportService = Substitute.For<IReportService>();
			_reportGenerator = Substitute.For<IReportGenerator>();
			_sut = new ReportController(_reportService, _reportGenerator);
		}

		private T GetReportResult<T>(DateTime? from, DateTime? to) where T : class
		{
			var result = _sut.ByDate(null, null).Result as ViewResult;

			result.Should().NotBeNull();
			var model = result?.Model as T;

			model.Should().NotBeNull();
			return model;
		}

		[Fact]
		public void Index_returns_report_for_current_month_if_no_date_set()
		{
			var model = GetReportResult<Report>(null, null);

			var currentYear = DateTime.Today.Year;
			var currentMonth = DateTime.Today.Month;

			model.From.Should().Be(new DateTime(currentYear, currentMonth, 1));
			model.To.Should().Be(DateTime.Today);
		}

		[Fact]
		public void Index_returns_report_for_specified_dates()
		{
			_reportService.GetCallCount(Arg.Any<DateTime>(), Arg.Any<DateTime>())
				.Returns(Threading.Task.FromResult(2));

			_reportService.GetSmsCount(Arg.Any<DateTime>(), Arg.Any<DateTime>())
				.Returns(Threading.Task.FromResult(3));

			var topCallers = new List<TopNumber> {new TopNumber {Phone = "111222111", Count = 231}};
			_reportService.GetTopCallersByDuration(Arg.Any<DateTime>(), Arg.Any<DateTime>(), Arg.Any<int>())
				.Returns(Threading.Task.FromResult(topCallers));

			var topSmsers = new List<TopNumber> {new TopNumber {Phone = "555444999", Count = 12341}};
			_reportService.GetTopSmsersByCount(Arg.Any<DateTime>(), Arg.Any<DateTime>(), Arg.Any<int>())
				.Returns(Threading.Task.FromResult(topSmsers));

			var model = GetReportResult<Report>(new DateTime(2018, 1, 3), new DateTime(2018, 2, 14));

			model.CallDetails.Top.Should().BeSameAs(topCallers);
			model.CallDetails.Total.Should().Be(2);

			model.SmsDetails.Top.Should().BeSameAs(topSmsers);
			model.SmsDetails.Total.Should().Be(3);
		}

		[Fact]
		public void ByReport_returns_generate_data_if_calls_and_smses_count_is_0()
		{
			_reportService.GetCallCount(Arg.Any<DateTime>(), Arg.Any<DateTime>())
				.Returns(Threading.Task.FromResult(0));

			_reportService.GetSmsCount(Arg.Any<DateTime>(), Arg.Any<DateTime>())
				.Returns(Threading.Task.FromResult(0));

			var view = _sut.ByDate(null, null).Result as ViewResult;
			view.ViewName.Should().Be("GenerateData");
		}

		[Fact]
		public void ByReport_returns_wrong_range_if_from_is_after_to()
		{
			var view = _sut.ByDate(DateTime.Today, DateTime.Today.AddDays(-1)).Result as ViewResult;
			view.ViewName.Should().Be("WrongRange");
		}

		[Fact]
		public void Generate_should_call_data_generator_and_redirect_to_report()
		{
			var from = new DateTime(2010, 3, 2);
			var to = new DateTime(2010, 5, 1);

			var receivedFrom = DateTime.MaxValue;
			var receivedTo = DateTime.MinValue;
			int receivedCount = 0;

			_reportGenerator.Generate(Arg.Do<DateTime>(f => receivedFrom = f), Arg.Do<DateTime>(t => receivedTo = t),
				Arg.Do<int>(c => receivedCount = c)).Wait();

			var result = _sut.Generate(from, to).Result as RedirectToActionResult;

			result.Should().NotBeNull();
			result.ActionName.Should().Be(nameof(ReportController.ByDate));

			receivedFrom.Should().Be(from);
			receivedTo.Should().Be(to);
			receivedCount.Should().Be(1000);
		}
	}
}