﻿using System;
using FluentAssertions;
using Microsoft.EntityFrameworkCore.Storage;
using Task.Web.Data;
using Task.Web.Data.Models;
using Task.Web.Models;
using Task.Web.Services;
using Xunit;

namespace Task.Tests
{
	public abstract class ReportServiceTests : IDisposable
	{
		protected IReportService ReportService;
		
		protected readonly ReportDbContext ReportDbContext;
		protected readonly IDbContextTransaction Transaction;

		private const int FirstPhone = 100000000;
		private const int SecondPhone = 200000000;
		private const int ThirdPhone = 300000000;
		private const int FourthPhone = 400000000;
		
		private static readonly DateTime From = new DateTime(1999,2,1);
		private static readonly DateTime To = new DateTime(1999,2,5);

		protected ReportServiceTests()
		{
			ReportDbContext = DbContextProvider.GetContext();
			Transaction = ReportDbContext.Database.BeginTransaction();
			
			Seed();
		}

		private void Seed()
		{
			// calls
			// not included
			ReportDbContext.Calls.Add(new CallEvent{Msisdn = FirstPhone, Date = From.AddHours(-1), Duration = 200 });
			ReportDbContext.Calls.Add(new CallEvent{Msisdn = FirstPhone, Date = To.AddHours(1), Duration = 200 });
			
			// included
			ReportDbContext.Calls.Add(new CallEvent{Msisdn = FourthPhone, Date = From, Duration = 4 });
			
			ReportDbContext.Calls.Add(new CallEvent{Msisdn = ThirdPhone, Date = From, Duration = 4 });
			ReportDbContext.Calls.Add(new CallEvent{Msisdn = ThirdPhone, Date = From.AddHours(1), Duration = 2 });
			
			ReportDbContext.Calls.Add(new CallEvent{Msisdn = SecondPhone, Date = From, Duration = 40 });
			ReportDbContext.Calls.Add(new CallEvent{Msisdn = SecondPhone, Date = From.AddHours(1), Duration = 20 });
			
			ReportDbContext.Calls.Add(new CallEvent{Msisdn = FirstPhone, Date = From, Duration = 200 });
			ReportDbContext.Calls.Add(new CallEvent{Msisdn = FirstPhone, Date = From.AddDays(1), Duration = 400 });
			ReportDbContext.Calls.Add(new CallEvent{Msisdn = FirstPhone, Date = To, Duration = 300 });

			// sms
			// not included 
			ReportDbContext.Smses.Add(new SmsEvent{Msisdn = FirstPhone, Date = From.AddDays(-1)});
			ReportDbContext.Smses.Add(new SmsEvent{Msisdn = FirstPhone, Date = To.AddDays(1)});
			
			// included
			ReportDbContext.Smses.Add(new SmsEvent{Msisdn = FirstPhone, Date = From});
			ReportDbContext.Smses.Add(new SmsEvent{Msisdn = FirstPhone, Date = From.AddHours(1)});
			ReportDbContext.Smses.Add(new SmsEvent{Msisdn = FirstPhone, Date = From.AddDays(1)});
			ReportDbContext.Smses.Add(new SmsEvent{Msisdn = FirstPhone, Date = To});
			
			ReportDbContext.Smses.Add(new SmsEvent{Msisdn = SecondPhone, Date = From.AddHours(1)});
			ReportDbContext.Smses.Add(new SmsEvent{Msisdn = SecondPhone, Date = From.AddDays(1)});
			ReportDbContext.Smses.Add(new SmsEvent{Msisdn = SecondPhone, Date = To});
			
			ReportDbContext.Smses.Add(new SmsEvent{Msisdn = ThirdPhone, Date = From.AddHours(1)});
			ReportDbContext.Smses.Add(new SmsEvent{Msisdn = ThirdPhone, Date = From.AddDays(1)});
			
			ReportDbContext.Smses.Add(new SmsEvent{Msisdn = FourthPhone, Date = From.AddDays(1)});

			ReportDbContext.SaveChanges();
		}
		
		public void Dispose()
		{
			Transaction.Dispose();
			ReportDbContext.Dispose();
		}
		
		[Fact]
		public void Gets_calls_cound_for_period()
		{
			var calls = ReportService.GetCallCount(From, To).Result;
			calls.Should().Be(8);
		}
		
		[Fact]
		public void Gets_top_callers_by_duration()
		{
			var topCalls = ReportService.GetTopCallersByDuration(From, To, 3).Result;
			
			topCalls.Should().BeEquivalentTo(
				new TopNumber{Phone = FirstPhone.ToString(), Count = 900},
				new TopNumber{Phone = SecondPhone.ToString(), Count = 60},
				new TopNumber{Phone = ThirdPhone.ToString(), Count = 6}
			);
		}
		
		[Fact]
		public void Gets_sms_cound_for_period()
		{
			var calls = ReportService.GetSmsCount(From, To).Result;
			calls.Should().Be(10);
		}
		
		[Fact]
		public void Gets_top_smsers()
		{
			var topCalls = ReportService.GetTopSmsersByCount(From, To, 3).Result;
			
			topCalls.Should().BeEquivalentTo(
				new TopNumber{Phone = FirstPhone.ToString(), Count = 4},
				new TopNumber{Phone = SecondPhone.ToString(), Count = 3},
				new TopNumber{Phone = ThirdPhone.ToString(), Count = 2}
			);
		}
	}
}