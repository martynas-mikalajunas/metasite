﻿using System;
using Microsoft.AspNetCore.Mvc;
using Task.Web.Models;
using Task.Web.Services;

namespace Task.Web.Controllers
{
	public class ReportController : Controller
	{
		private readonly IReportService _reportService;
		private readonly IReportGenerator _reportGenerator;

		private const int TopDisplayNumber = 5;
		
		public ReportController(IReportService reportService, IReportGenerator reportGenerator)
		{
			_reportService = reportService;
			_reportGenerator = reportGenerator;
		}
		
		// GET
		public async System.Threading.Tasks.Task<IActionResult> ByDate(DateTime? from, DateTime? to)
		{
			var normalizedFrom = from ?? new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1);
			var normalizedTo = to ?? DateTime.Today;

			if (normalizedFrom > normalizedTo)
				return View("WrongRange");
			
			var model = new Report
			{
				From = normalizedFrom,
				To = normalizedTo,
				CallDetails = new ReportDetails
				{
					Header = Report.PhoneHeader,
					TopsHeader = Report.TopPhoneHeader,
					TotalsHeader = Report.TotalsPhoneHeader,
					Img = Report.PhoneImg,
					Total =  await _reportService.GetCallCount(normalizedFrom, normalizedTo),
					Top =  await _reportService.GetTopCallersByDuration(normalizedFrom, normalizedTo, TopDisplayNumber )
				},
				SmsDetails = new ReportDetails
				{
					Header = Report.SmsHeader,
					TopsHeader = Report.TopSmsHeader,
					TotalsHeader = Report.TotalsSmsHeader,
					Img = Report.SmsImg,
					Total =  await _reportService.GetSmsCount(normalizedFrom, normalizedTo),
					Top =  await _reportService.GetTopSmsersByCount(normalizedFrom, normalizedTo, TopDisplayNumber )
				}
			};

			if (model.CallDetails.Total == 0 && model.SmsDetails.Total == 0)
				return View("GenerateData", model);
			
			return View(model);
		}

		[HttpPost]
		public async System.Threading.Tasks.Task<IActionResult> Generate(DateTime from, DateTime to)
		{
			await _reportGenerator.Generate(from, to, 1000);
			return RedirectToAction("ByDate", 
				new
				{
					From = from.ToString("yyyy-MM-dd"), 
					To = to.ToString("yyyy-MM-dd")
				});
		}
	}
}