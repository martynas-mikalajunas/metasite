﻿using System;

namespace Task.Web.Data.Models
{
	public class CallEvent
	{
		public int Msisdn { get; set; }
		public int Duration { get; set; }
		public DateTime Date { get; set; }
	}
}