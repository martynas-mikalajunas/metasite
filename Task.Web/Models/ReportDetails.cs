﻿using System.Collections.Generic;

namespace Task.Web.Models
{
	public class ReportDetails
	{
		public string Header { get; set; }
		public string Img { get; set; }
		public string TotalsHeader { get; set; }
		public string TopsHeader { get; set; }
		public int Total { get; set; }
		public IEnumerable<TopNumber> Top{ get; set; }
	}
}