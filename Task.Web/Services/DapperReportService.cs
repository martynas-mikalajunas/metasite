﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using Task.Web.Models;

namespace Task.Web.Services
{
	public class DapperReportService : IReportService
	{
		private readonly IDbConnectionProvider _cnnProvider;

		public DapperReportService(IDbConnectionProvider cnnProvider)
		{
			_cnnProvider = cnnProvider;
		}

		private Task<T> Q<T>(string sql, object param)
		{
			return _cnnProvider.Connection.QueryFirstAsync<T>(sql, param, _cnnProvider.Transaction);
		}
		
		private async Task<List<T>> Qs<T>(string sql, object param)
		{
			var r = await _cnnProvider.Connection.QueryAsync<T>(sql, param, _cnnProvider.Transaction);
			return r.ToList();
		}
		
		public Task<int> GetCallCount(DateTime from, DateTime to)
		{
			return Q<int>("select count(*) from Calls where Date between @from and @to", new{from, to});
		}

		public Task<int> GetSmsCount(DateTime @from, DateTime to)
		{
			return Q<int>("select count(*) from Smses where Date between @from and @to", new{from, to});
		}

		public Task<List<TopNumber>> GetTopCallersByDuration(DateTime from, DateTime to, int number)
		{
			return Qs<TopNumber>(
				"select top (@top) Msisdn as Phone, sum(duration) as Count " +
				"from Calls " +
				"where Date between @from and @to " +
				"group by Msisdn " +
				"order by 2 desc", 
				new {from, to, top = number});
		}

		public Task<List<TopNumber>> GetTopSmsersByCount(DateTime from, DateTime to, int number)
		{
			return Qs<TopNumber>(
				"select top (@top) Msisdn as Phone, count(*) as Count " +
				"from Smses " +
				"where Date between @from and @to " +
				"group by Msisdn " +
				"order by 2 desc", 
				new {from, to, top = number});
		}
	}
}