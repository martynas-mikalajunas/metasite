﻿using System;

namespace Task.Web.Data.Models
{
	public class SmsEvent
	{
		public int Msisdn { get; set; }
		public DateTime Date { get; set; }
	}
}