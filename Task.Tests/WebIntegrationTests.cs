﻿using System.IO;
using System.Linq;
using System.Net;
using MS = System.Threading.Tasks;
using System.Net.Http;
using System.Reflection;
using FluentAssertions;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Razor;
using Microsoft.AspNetCore.TestHost;
using Microsoft.CodeAnalysis;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.PlatformAbstractions;
using Task.Web;
using Xunit;

namespace Task.Tests
{
	public class WebIntegrationTests
	{
		private readonly HttpClient _httpClient;

		public WebIntegrationTests()
		{
			var testServer = new TestServer(
				new WebHostBuilder()
					.UseConfiguration(TestConfig.Configuration)
#region Microsoft is not so good with testing :(
					.UseContentRoot(
						Path.Combine(
							PlatformServices.Default.Application.ApplicationBasePath,
							"../../../../",
							"Task.Web"))
					.ConfigureServices(services =>
					{
						services.Configure((RazorViewEngineOptions options) =>
						{
							var previous = options.CompilationCallback;
							options.CompilationCallback = (context) =>
							{
								previous?.Invoke(context);

								var assembly = typeof(Startup).GetTypeInfo().Assembly;
								var assemblies = assembly.GetReferencedAssemblies()
									.Select(x => MetadataReference.CreateFromFile(Assembly.Load(x).Location))
									.ToList();
								
								assemblies.Add(MetadataReference.CreateFromFile(Assembly.Load(new AssemblyName("mscorlib")).Location));
								assemblies.Add( MetadataReference.CreateFromFile(Assembly.Load(new AssemblyName("System.Private.Corelib")).Location));
								assemblies.Add(MetadataReference.CreateFromFile(Assembly.Load(new AssemblyName("netstandard")).Location)); 
								assemblies.Add(MetadataReference.CreateFromFile(Assembly.Load(new AssemblyName("System.Linq")).Location)); 
								assemblies.Add(MetadataReference.CreateFromFile(Assembly.Load(new AssemblyName("System.Threading.Tasks")).Location)); 
								assemblies.Add(MetadataReference.CreateFromFile(Assembly.Load(new AssemblyName("System.Runtime")).Location)); 
								assemblies.Add(MetadataReference.CreateFromFile(Assembly.Load(new AssemblyName("System.Dynamic.Runtime")).Location)); 
								assemblies.Add(MetadataReference.CreateFromFile(Assembly.Load(new AssemblyName("Microsoft.AspNetCore.Razor.Runtime")).Location)); 
								assemblies.Add(MetadataReference.CreateFromFile(Assembly.Load(new AssemblyName("Microsoft.CSharp")).Location)); 
								assemblies.Add(MetadataReference.CreateFromFile(Assembly.Load(new AssemblyName("Microsoft.AspNetCore.Mvc")).Location)); 
								assemblies.Add(MetadataReference.CreateFromFile(Assembly.Load(new AssemblyName("Microsoft.AspNetCore.Razor")).Location));
								assemblies.Add(MetadataReference.CreateFromFile(Assembly.Load(new AssemblyName("Microsoft.AspNetCore.Mvc.Razor")).Location));
								assemblies.Add(MetadataReference.CreateFromFile(Assembly.Load(new AssemblyName("Microsoft.AspNetCore.Html.Abstractions")).Location));
								assemblies.Add(MetadataReference.CreateFromFile(Assembly.Load(new AssemblyName("System.Text.Encodings.Web")).Location));
								context.Compilation = context.Compilation.AddReferences(assemblies);
							};
						});
					})
#endregion
					.UseStartup<Startup>());
			_httpClient = testServer.CreateClient();
		}

		[Fact]
		public async MS.Task Get_report_is_working()
		{
			var response = await _httpClient.GetAsync("/Report/ByDate?from=2018-03-01&to=2018-05-01");
			response.StatusCode.Should().Be(HttpStatusCode.OK);
		}
	}
}