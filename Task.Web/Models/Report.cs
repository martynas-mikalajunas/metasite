﻿using System;

namespace Task.Web.Models
{
	public class Report : ReportFilter
	{
		public const string SmsHeader = "SMS";
		public const string PhoneHeader = "Phone";
		
		public const string TotalsSmsHeader = "Total sms'es";
		public const string TotalsPhoneHeader = "Total calls";
		
		public const string TopSmsHeader = "Top by SMS count";
		public const string TopPhoneHeader = "Top calls by duration";

		public const string PhoneImg = "call.jpg";
		public const string SmsImg = "sms.jpg";
		
		public ReportDetails CallDetails { get; set; }
		public ReportDetails SmsDetails { get; set; }
	}
}