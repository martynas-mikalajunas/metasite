using System;
using System.Linq;
using FluentAssertions;
using Microsoft.EntityFrameworkCore.Storage;
using Task.Web.Data;
using Task.Web.Data.Models;
using Xunit;

namespace Task.Tests
{
	public class DbContextTests : IDisposable
	{
		private readonly ReportDbContext _sut;
		private readonly IDbContextTransaction _transaction;

		public DbContextTests()
		{
			_sut = DbContextProvider.GetContext();
			_transaction = _sut.Database.BeginTransaction();
		}

		public void Dispose()
		{
			_transaction.Dispose();
			_sut.Dispose();
		}

		[Fact]
		public void Calls_entity_working()
		{
			var call = new CallEvent {Msisdn = 869964970, Duration = 12, Date = DateTime.Now};

			_sut.Calls.Add(call);
			_sut.SaveChanges();
			_sut.Calls.Where(c => c.Msisdn == 869964970).Should().BeEquivalentTo(call);
		}
		
		[Fact]
		public void Sms_entity_working()
		{
			var sms = new SmsEvent {Msisdn = 869964970, Date = DateTime.Now};

			_sut.Smses.Add(sms);
			_sut.SaveChanges();
			_sut.Smses.Where(c => c.Msisdn == 869964970).Should().BeEquivalentTo(sms);
		}
	}
}