﻿using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Task.Web.Data;

namespace Task.Web
{
	public static class Program
	{
		public static void Main(string[] args)
		{
			var host = BuildWebHost(args);
			
			using (var scope = host.Services.CreateScope())
			using (var context = scope.ServiceProvider.GetService<ReportDbContext>())
			{
				context.Database.Migrate();
			}

			host.Run();
		}

		private static IWebHost BuildWebHost(string[] args) =>
			WebHost.CreateDefaultBuilder(args)
				.UseStartup<Startup>()
				.Build();
	}
}