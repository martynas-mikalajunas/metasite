# Sukurti web aplikaciją:

## Reikalavimai funkcionalumui

1. Galimybė prisijungti prie MSSQL DB, sukurti atitinkamą duomenų bazės struktūrą (jei tokia neegzistuoja).

1. DB struktūrą savo nuožiūra suformuoti pagal pateiktą duomenų modelį (žr. punktą 1.3), užpildyti atsitiktiniais duomenimis, atsižvelgiant į tai, jog:

	1.  reikia sukurti web formą su datos rėžio pasirinkimu;

	1. forma turi atfiltruoti šiuos duomenis:

		* kiek viso (type="sms") per pasirinktą laikotarpį;
 		* kiek viso (type="call") per pasirinktą laikotarpį;
		* top5 msisdn (type="sms") per pasirinktą laikotarpį;
		* top5 msisdn (type="call") pagal "duration" per pasirinktą laikotarpį.

1. Duomenų struktūros:

	| msisdn | type | date |
	|--------|------|------|
	|37062300001 | sms | 3/1/2018 8:01:01 AM |
	|37062300001 | sms | 3/1/2018 10:40:22 PM|

	....

	|  msisdn    | type | duration (seconds) | date  |
	|------------|------|-----|----------------------|
	|37062300002 | call | 32  | 3/1/2018 0:10:10 AM  |
	|37062300001 | call | 187 | 3/1/2018 0:10:07 PM  |
	|37062300003 | call | 44  | 3/1/2018 4:14:01 AM  |


## Reikalavimai užduoties išpildymui

- Panaudoti ASP.NET Core, EF core, MSSQL DB.
- Kitas technologijas (pvz. frontend development) galima rinktis laisvai.
- Pateiktas programinis kodas turi būti tvarkingas, lengvai skaitomas.
- DB struktūros atskirai pateikti nereikia, pakankama implementacija programiniame kode.
- Panaudotos gerosios programavimo paradigmos, pritaikomi programinio kodo perpanaudojimo, paprastumo ir kiti pagrindiniai principai.
