﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Task.Web.Data;

namespace Task.Tests
{
	public static class DbContextProvider
	{
		public static ReportDbContext GetContext()
		{
			var builder = new DbContextOptionsBuilder<ReportDbContext>();
			builder.UseSqlServer(TestConfig.Configuration.GetConnectionString("default"));
			var contextOptions = builder.Options;
			return new ReportDbContext(contextOptions);
		}
	}
}