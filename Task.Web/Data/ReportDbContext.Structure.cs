using Microsoft.EntityFrameworkCore;
using Task.Web.Data.Models;

namespace Task.Web.Data
{
	public partial class ReportDbContext
	{
		protected override void OnModelCreating(ModelBuilder modelBuilder)
		{
			modelBuilder.Entity<CallEvent>()
				.HasKey(ce => new {ce.Date, ce.Msisdn});
			
			modelBuilder.Entity<SmsEvent>()
				.HasKey(se => new {se.Date, se.Msisdn});
		}
	}
}