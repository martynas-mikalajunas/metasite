﻿using System;
using System.Data.Common;
using System.Data.SqlClient;

namespace Task.Web.Services
{
	public interface IDbConnectionProvider
	{
		DbConnection Connection { get; }
		DbTransaction Transaction { get; }
	}
	
	public class DbConnectionProvider : IDbConnectionProvider, IDisposable
	{
		private readonly string _cnnStr;
		private DbConnection _connection;

		public DbConnectionProvider(string cnnStr)
		{
			_cnnStr = cnnStr;
			Transaction = null;
		}

		public DbConnection Connection
		{
			get
			{
				if (null == _connection)
				{
					_connection = new SqlConnection(_cnnStr);
					_connection.Open();
				}
				
				return _connection;
			}
		}

		public DbTransaction Transaction { get; }

		public void Dispose()
		{
			Connection?.Dispose();
			Transaction?.Dispose();
		}
	}
}