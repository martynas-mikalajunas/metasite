﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Task.Web.Data;
using Task.Web.Models;

namespace Task.Web.Services
{
	public class EfReportService : IReportService
	{
		private readonly ReportDbContext _context;

		public EfReportService(ReportDbContext context)
		{
			_context = context;
		}
		
		public Task<int> GetCallCount(DateTime from, DateTime to)
		{
			return _context
				.Calls.Where(c => c.Date >= from && c.Date <= to)
				.CountAsync();
		}

		public Task<int> GetSmsCount(DateTime from, DateTime to)
		{
			return _context
				.Smses.Where(c => c.Date >= from && c.Date <= to)
				.CountAsync();
		}

		public Task<List<TopNumber>> GetTopCallersByDuration(DateTime from, DateTime to, int number)
		{
			return _context.Calls
				.Where(c => c.Date >= from && c.Date <= to)
				.GroupBy(g => g.Msisdn)
				.Select(g => new TopNumber {Phone = g.Key.ToString(), Count = g.Sum(i => i.Duration)})
				.OrderByDescending(g => g.Count)
				.Take(number)
				.ToListAsync();
		}

		public Task<List<TopNumber>> GetTopSmsersByCount(DateTime from, DateTime to, int number)
		{
			return _context.Smses
				.Where(c => c.Date >= from && c.Date <= to)
				.GroupBy(g => g.Msisdn)
				.Select(g => new TopNumber {Phone = g.Key.ToString(), Count = g.Count()})
				.Take(number)
				.ToListAsync();
		}
	}
}