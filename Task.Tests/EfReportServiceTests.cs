﻿using Task.Web.Services;

namespace Task.Tests
{
	public class EfReportServiceTests : ReportServiceTests
	{
		public EfReportServiceTests()
		{
			ReportService = new EfReportService(ReportDbContext);
		}
	}
}