﻿using System.IO;
using Microsoft.Extensions.Configuration;

namespace Task.Tests
{
	public static class TestConfig
	{
		private static IConfiguration _configuration;

		public static IConfiguration Configuration
		{
			get
			{
				if (null == _configuration)
				{
					var builder = new ConfigurationBuilder()
						.SetBasePath(Directory.GetCurrentDirectory())
						.AddJsonFile("appsettings.json");

					_configuration = builder.Build();
				}

				return _configuration;
			}
		}
	}
}