﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using NSubstitute;
using Task.Web.Services;

namespace Task.Tests
{
	public class DapperReportServiceTests : ReportServiceTests
	{
		public DapperReportServiceTests()
		{
			var cnnProvider = Substitute.For<IDbConnectionProvider>();
			
			cnnProvider.Connection.Returns(ReportDbContext.Database.GetDbConnection());
			cnnProvider.Transaction.Returns(Transaction.GetDbTransaction());

			ReportService = new DapperReportService(cnnProvider);
		}
	}
}