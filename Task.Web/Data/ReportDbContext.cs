﻿using Microsoft.EntityFrameworkCore;
using Task.Web.Data.Models;

namespace Task.Web.Data
{
	public partial class ReportDbContext : DbContext
	{
		public ReportDbContext(DbContextOptions<ReportDbContext> options)
			: base(options)
		{
		}

		public DbSet<CallEvent> Calls { get; set; }
		public DbSet<SmsEvent> Smses { get; set; }
	}
}