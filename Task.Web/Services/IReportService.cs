﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Task.Web.Models;

namespace Task.Web.Services
{
	public interface IReportService
	{
		Task<int> GetCallCount(DateTime from, DateTime to);
		Task<int> GetSmsCount(DateTime from, DateTime to);
		Task<List<TopNumber>> GetTopCallersByDuration(DateTime from, DateTime to, int number);
		Task<List<TopNumber>> GetTopSmsersByCount(DateTime from, DateTime to, int number);
	}
}