﻿using System;

namespace Task.Web.Models
{
	public class ReportFilter
	{
		public DateTime? From { get; set; }
		public DateTime? To { get; set; }
	}
}