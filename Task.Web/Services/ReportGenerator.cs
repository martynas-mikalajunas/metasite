﻿using System;
using Task.Web.Data;
using Task.Web.Data.Models;

namespace Task.Web.Services
{
	public interface IReportGenerator
	{
		System.Threading.Tasks.Task Generate(DateTime from, DateTime to, int count);
	}
	
	public class ReportGenerator : IReportGenerator
	{
		private readonly ReportDbContext _context;
		private static readonly int[] PhoneNumbers = {
			869912578,
			869913578,
			869912378,
			869932578,
			869312578,
			863912578,
			863912378,
			869932578,
			869312578,
			869915578,
			865952578,
			865912578,
			869515558,
			869912555,
			869915555,
		};

		public ReportGenerator(ReportDbContext context)
		{
			_context = context;
		}
		public System.Threading.Tasks.Task Generate(DateTime from, DateTime to, int count)
		{
			var rnd = new Random();

			var diffInSecs = to.AddHours(24) - from;
			var step = diffInSecs.TotalMilliseconds / count;
			var currentDate = from;
			
			for (int i = 0; i < count; ++i)
			{
				var phone = PhoneNumbers[rnd.Next(0, PhoneNumbers.Length - 1)];

				_context.Calls.Add(new CallEvent {Msisdn = phone, Date = currentDate, Duration = rnd.Next(20, 1500)});
				
				if(rnd.Next(30)%3 == 0)
					_context.Smses.Add(new SmsEvent {Msisdn = phone, Date = currentDate});

				currentDate = currentDate.AddMilliseconds(step);
			}

			return _context.SaveChangesAsync();
		}
	}
}